import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),

  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/index',
    name: 'Index',
    component: () => import('../views/Index.vue'),
    children: [
      {
        path: '/index/show',
        name: 'Show',
        component: () => import('../views/Home/Show.vue')
      },
      {
        path: '/index/car',
        name: 'Car',
        component: () => import('../views/Home/Car.vue')
      },
      {
        path: '/index/sort',
        name: 'Sort',
        component: () => import('../views/Home/Sort.vue')
      },
      {
        path: '/index',
        redirect: '/index/sort'
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
