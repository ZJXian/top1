const Koa = require('koa');
// app服务应用
const app = new Koa();

const open = require('open')
// 挂载中间件
const static = require('koa-static')
const bodyparser = require('koa-bodyparser')

const Router = require("koa-router")
// 2.实例
const router = new Router()

app.use(static("./public"));




router.post("/api/login", async (ctx) => {
    console.log(ctx.request.body);
    ctx.body = {
        code: 200
    }
})
// app.use(async (app, next) => {
//     if (app.request.url === '/a.js') {
//         app.body = fs.readFileSync('./public/a.js', 'utf-8')
//         return
//     }
// })

// app.use(async ({ request, res }, next) => {
//     if (request.url === '/api/getdata') {
//         res.body = {
//             code: 200,
//             data: {
//                 list: [
//                     {
//                         name: "1"
//                     }, {
//                         name: "2"
//                     }
//                 ]
//             }
//         }
//     }
//     return
// })

app.use(bodyparser());

app.use(router.routes(), router.allowedMethods())

app.listen(3000, () => {
    console.log('http://localhost:3000');
})