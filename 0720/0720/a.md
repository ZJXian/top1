## 发包
   登录账号
npm login 登录  输入密码 账号 邮箱
npm publish 发布  
### 把cnpm改变为原来的npm
淘宝镜像
npm config set registry https://registry.npmjs.org

### 第一天
命令行工具的编写
1. 创建文件  
2. npm init -y   
3. 配置bin字段 
4. #! /usr/bin/env node
5. npm i -g 下包
6.  执行命令行  包名

## 第二天
天气案例 数据请求

## 第三天
inquirer commander案例的增删改查(表格)
fs path 拷贝文件/文件夹

## 安装git   第四天
本地工作区 git init  必须有.git文件夹才能被git管理
暂存区git add . (必须带有空格)
历史版本区git commit -m "自己的提交信息" 
cmder  控制面板

git log打印日志  (查看提交记录 )
git reflog (查看所有提交记录 )
git reset --hard 粘贴commit路径   (返回特定版本)
cat 文件

cd进入文件 touch创建文件
git init 初始化
git add . 到暂存区
git commit -m "l"  到历史版本区
touch文件名   创建文件
ls查看所有文件
ls -a 查看隐藏的 .git

如果出现 Please tell me who you are.
则
git config --global user.email "你的邮箱"
git config --global user.name "你的名字"

git config user.name “username”，
换行输入   git config user.email “email”

## 创建仓库
git 创建仓库 选择: 开源 Nodejs  Node ISC
git clone 远程仓库地址
cd 进入创建的仓库
在仓库里新建文件夹 里面存放文件
git add .
git commit -m "first"
git push 向远程仓库推送代码

git status 查看仓库状态

凭据管理器

查看信息  密码和邮箱
git config --list 

## 第五天 koa   express的延伸
Koa是一个新的web框架，由Express幕后的原班人马打造，致力于成为 web应用和API开发领域中的一个更小、更富有表现力、更健壮的基石。通过利用async函数，Koa帮你丢弃回调函数，并有力地增强错误处理。Koa并没有捆绑任何中间件，而是提供了一套优雅的方法，帮助您快速而愉快地编写服务端应用程序。


下包 npm i koa
koa的内置中间件 use  
npm i open     自动打开浏览器
中间件必须是async函数
async函数返回值是promise对象

1. npm install koa
2. new Koa() 得到server服务器对象
3. server.listen 监听端口
4. node 服务文件启动

200-300 成功
300-400 缓存 304
400-500 请求错误
500 服务错误

koa-static中间件  处理静态资源请求

url 请求方式 返回值

url地址的组成部分
http://localhost:8080/api/getdata?page=1#a
协议 127.0.0.1 端口  pathname  query get请求 hash模式

格式化url地址  url.parse
处理请求参数 koa-body-parser


解决跨越问题
1、webpack  proxy 反向代理 
两个服务器之间的跨域处理
2、
