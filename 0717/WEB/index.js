#! /usr/bin/env node
// console.log('11');

// 一、1、正确编写命令行命令
// 2、正确下载inquirer和commander包
// 3、正确使用inquirer包完成交互
// 4、问题：输入需要删除文件
// 5、通过箭头选择是否确定删除
// 6、正确使用commander包完成一个可递归删除目录的命令
// 7、正确获取命令行参数
// 8、正确对命令行参数进行容错处理
// 9、判断目录是否存在，做出友好提示
const inquirer = require("inquirer")
const fs = require('fs')
const path = require('path')
const commander = require('commander')

const removeFile = (fileName) => {
    const filePath = path.join(process.cwd(), fileName)
    // 文件路径
    console.log(filePath);
    if (!fs.existsSync(filePath)) {
        console.log('文件不存在');
        return
    } else {
        let a = fs.statSync(filePath)
        // console.log(a.isDirectory());
        // // 判断文件夹
        if (a.isFile()) {
            fs.unlinkSync(filePath)
        } else {
            let list = fs.readdirSync(filePath)
            console.log(list);
            if (list.length > 0) {
                list.forEach(item => {
                    console.log(item);
                    removeFile(path.join(fileName, item))
                })
            }

            fs.rmdirSync(filePath)
            console.log('文件删除成功');
        }

    }
}

inquirer.prompt([
    {
        type: "confirm",
        name: "确定要删除吗"
    },
    {
        type: 'input',
        name: "请输入文件名"
    }
]).then(res => {
    const fileName = res['请输入文件名'];
    console.log(fileName);
    removeFile(fileName)
})