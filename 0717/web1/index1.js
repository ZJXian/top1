#! /usr/bin/env node
console.log('11');
// 10、正确使用nodejs内置fs、path模块
// 11、正确编写 自己的名字_exchange 命令
// 12、正确实现交互提示用户输入两个文件路径
// 13、通过commander判断参数是否为2
// 14、不是则结束命令并提示
// 15、正确展示两个文件的内容
// 16、正确展示文件的信息(大小、类型等)
// 17、正确交换两个文件的内容
// 18、通过把两个文件内容合并生成一个新的文件
// 19、判断新文件名字是否当前目录存在做出对应处理
// 20、正确展示新文件
// 21、正确发布在npm官网
// 22、正确从npm官网下载自己发布的包
// 23、测试自己下的包
// 24、代码书写规范并添加注释
// 25、在效果录屏充分展示自己的效果并讲解

const inquirer = require("inquirer")
const fs = require('fs')
const path = require('path')
const commander = require('commander')